from trytond.pool import PoolMeta


class Cron(metaclass=PoolMeta):
    __name__ = 'ir.cron'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.method.selection.extend([
                ('party.party|synchronice_party_conlab',
                    "Update Parties on Conlab"),
                ('laboratory.order|task_process_orders',
                    "Process Orders Lab State Draft"),
                ('laboratory.order|task_process_invoice',
                    "Process Invoice from Order Lab State"),
                ('laboratory.order|task_reconcile_orders',
                    "Reconcile Orders paid"),
                ])
