# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import json
from datetime import datetime, timedelta

import requests
from sql.operators import Not, Or
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction

HEADERS = {
           'Accept': 'application/json',
           'Content-type': 'application/json',
           }


class Party(ModelSQL, ModelView):
    __name__ = 'party.party'
    ext_id = fields.Char('Ext Id')
    last_update = fields.Date('Last update')

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls.name.states = {
            'required': Eval('is_patient', False),
        }
        cls.type_document.states = {
            'required': Eval('is_patient', False),
        }
        cls.id_number.states = {
            'required': Eval('is_patient', False),
        }
        cls.sex.states = {
            'required': Eval('is_patient', False),
        }
        cls.birthday.states = {
            'required': Eval('is_patient', False),
        }
        cls.born_country.states = {
            'required': Eval('is_patient', False),
        }

    @classmethod
    def synchronice_party_conlab(cls):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Move = pool.get('account.move')
        Account = pool.get('account.account')
        AccountType = pool.get('account.account.type')
        Invoice = pool.get('account.invoice')
        Payment = pool.get('account.invoice.payment_term')
        Date = pool.get('ir.date')
        party = cls.__table__()
        cursor = Transaction().connection.cursor()
        context = Transaction().context
        account = Account.__table__()
        move_line = MoveLine.__table__()
        move = Move.__table__()
        account_type = AccountType.__table__()
        payment = Payment.__table__()
        invoice = Invoice.__table__()

        today = Date.today()
        from_date = datetime.now() - timedelta(minutes=30)

        where = payment.payment_type == '2'
        # where &= Or([party.last_update < today, party.last_update == Null])
        query = invoice.join(payment, condition=invoice.payment_term == payment.id,
            ).join(party, condition=invoice.party == party.id,
            ).select(invoice.party, distinct=True,
            where=where)

        cursor.execute(*query)
        result = cursor.fetchall()

        parties = [row[0] for row in result]
        where = account_type.receivable
        where &= Not(account.closed)
        where &= move.state == 'posted'
        where &= party.id.in_(parties)
        where &= Or([move.write_date >= from_date, move.create_date >= from_date])

        query = move_line.join(move, condition=move_line.move == move.id,
            ).join(account, condition=move_line.account == account.id,
            ).join(account_type, condition=account_type.id == account.type,
            ).join(party, condition=party.id == move_line.party,
            ).select(move_line.party, distinct=True,
            where=where)

        cursor.execute(*query)
        result = cursor.fetchall()
        parties = [row[0] for row in result]
        parties_ = cls.browse(parties)
        for party in parties_:
            is_person = True
            if party.type_document == '31':
                is_person = False
            params = {
                    'is_person': is_person,
                    'id_number': party.id_number,
                    'total_credit': str(party.receivable),
            }

            request = json.dumps(params)
            uri = 'http://d4450c187272.sn.mynetname.net:18001/cupoempresa'
            response = requests.post(uri, auth=('tryton', 'try70#Pc&1'),
                headers=HEADERS, data=request)
            if response.status_code == 200:
                party.last_update = today
                party.save()
