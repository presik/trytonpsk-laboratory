# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields


class Category(ModelSQL, ModelView):
    "Laboratory Category"
    __name__ = "laboratory.category"
    name = fields.Char('Name', required=True, translate=True)
    parent = fields.Many2One('laboratory.category', 'Parent')
    childs = fields.One2Many('laboratory.category', 'parent', string='Children')
    healthprof = fields.Many2One('gnuhealth.healthprofessional', 'Health Professional',
        ondelete="RESTRICT")

    @classmethod
    def __setup__(cls):
        super(Category, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    # def get_rec_name(self, name):
    #     if self.parent:
    #         return self.parent.get_rec_name(name) + ' / ' + self.name
    #     else:
    #         return self.name
