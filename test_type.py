# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import DeactivableMixin, ModelSQL, ModelView, Workflow, fields


class TestType(Workflow, ModelSQL, ModelView, DeactivableMixin):
    "Test Type"
    __name__ = 'laboratory.test_type'
    _rec_name = 'product.name'
    code = fields.Char('Code')
    product = fields.Many2One('product.product', 'Product', required=True,
        domain=[
            ('template.type', '=', 'service'),
            ('template.salable', '=', True),
        ])
    category = fields.Many2One('laboratory.category', 'Category', required=True,
        )
    description = fields.Char('Description')
    healthprof = fields.Many2One('gnuhealth.healthprofessional', 'Health Professional',
        ondelete="RESTRICT")
    lead_time = fields.TimeDelta("Lead Time")

    def get_rec_name(self, name):
        if self.product:
            return self.product.rec_name

    @fields.depends('category', 'healthprof')
    def on_change_category(self):
        if self.category and self.category.healthprof:
            self.healthprof = self.category.healthprof
