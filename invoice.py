# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from trytond.model import ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, If
from trytond.transaction import Transaction

_ZERO = Decimal('0.00')


PLAN = {
    '': '',
    '01':	'Plan de beneficios en salud financiado con UPC',
    '02':	'Presupuesto máximo',
    '03':	'Prima EPS / EOC, no asegurados SOAT',
    '04':	'Cobertura Póliza SOAT',
    '05':	'Cobertura ARL',
    '06':	'Cobertura ADRES',
    '07':	'Cobertura Salud Pública',
    '08':	'Cobertura entidad territorial, recursos de oferta',
    '09':	'Urgencias población migrante',
    '10':	'Plan complementario en salud',
    '11':	'Plan medicina prepagada',
    '12':	'Otras pólizas en salud',
    '13':	'Cobertura Régimen Especial o Excepción',
    '14':	'Cobertura Fondo Nacional de Salud de las Personas Privadas de la Libertad',
    '15':	'Particular',
    }

PAYMENT_MODE = {
    '': '',
    '01':	'Pago individual por caso / Conjunto integral de atenciones / Paquete / Canasta.',
    '02':	'Pago global prospectivo.',
    '03':	'Pago por capitación.',
    '04':	'Pago por evento.',
    '05':	'Otra modalidad (específica)',
}


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    annexes = fields.Many2Many('account.invoice-laboratory.order', 'invoice',
        'order', 'annexes')
    copago = fields.Numeric('Copago', digits=(16, 2), depends=['annexes'])
    cuota_moderadora = fields.Numeric('Cuota Moderadora', digits=(16, 2), depends=['annexes'])
    order_authorization_number = fields.Char('Order Authorization Number')
    # copago_as_charge = fields.Function(fields.Boolean('copago_as_charge'), 'get_copago_as_charge')
    benefit_plan_coverage = fields.Selection('get_benefit_plan', 'Benefit Plan Coverage')
    payment_mode_health = fields.Selection('get_payment_mode_health', 'Payment Mode Health')
    policy_number = fields.Char('Policy Number')

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls.account.domain = [
            ('closed', '!=', True),
            ('company', '=', Eval('company', -1)),
            If(Eval('type') == 'out',
                ('type.statement', '=', 'balance'),
                ('type.payable', '=', True)),
            ]

    @classmethod
    def get_benefit_plan(cls):
        return [(k, v) for k, v in PLAN.items()]

    @classmethod
    def get_payment_mode_health(cls):
        return [(k, v) for k, v in PAYMENT_MODE.items()]

    @classmethod
    def get_lines_to_pay(cls, invoices, name):
        invoices = [inv for inv in invoices if (inv.account.type.receivable or inv.account.type.payable)]
        return super(Invoice, cls).get_lines_to_pay(invoices, name)

    def get_reconciled(self, name):
        if not self.lines_to_pay and self.state in ('posted', 'paid'):
            return self.invoice_date
        return super(Invoice, self).get_reconciled(name)

    # def _get_move_line(self, date, amount):
    #     line = super(Invoice, self)._get_move_line(date, amount)
    #     if self.copago and self.operation_type != 'SS-CUFE':
    #         line.debit = line.debit - self.copago
    #     return line

    # def get_copago_as_charge(self, name=None):
    #     Config = Pool().get('account.configuration')
    #     return Config(1).copago_as_charge

    # def get_payable_amount(self):
    #     payable_amount = super(Invoice, self).get_payable_amount()
    #     if self.copago and self.copago > 0 and self.copago_as_charge:
    #         payable_amount -= abs(self.copago)
    #     return payable_amount

    # def get_move(self):
    #     move = super(Invoice, self).get_move()
    #     Config = Pool().get('laboratory.configuration')
    #     config = Config.get_configuration()
    #     if (self.copago or self.cuota_moderadora) and self.operation_type != 'SS-CUFE' and config.copago_product:
    #         move.lines = [*list(move.lines), self.get_copago_line()]
    #     return move

    def get_copago_line(self):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Config = pool.get('laboratory.configuration')
        config = Config.get_configuration()
        line = MoveLine()
        line.amount_second_currency = None
        line.second_currency = None
        amount_ = (self.copago or 0) + (self.cuota_moderadora or 0)
        if self.total_amount <= 0:
            line.debit, line.credit = 0, -amount_
        else:
            line.debit, line.credit = amount_, 0

        line.operation_center = self.operation_center.id
        account = config.copago_product.template.account_category.account_revenue
        line.account = account
        if account.party_required:
            line.party = self.party
        line.description = config.copago_product.rec_name
        return line

    @classmethod
    def post(cls, invoices):
        super(Invoice, cls).post(invoices)
        post_advance_copago = [i for i in invoices if i.copago or i.cuota_moderadora]
        if post_advance_copago:
            cls._post_advance_copago(post_advance_copago)

    @classmethod
    def _post_advance_copago(cls, invoices):
        pool = Pool()
        Move = pool.get('account.move')
        for invoice in invoices:
            move = invoice.get_move_advance_copago()
            Move.save([move])
            Move.post([move])
            invoice.add_payment_lines({invoice: [ln for ln in move.lines if ln.account == invoice.account]})

    def get_move_advance_copago(self):
        """
        Compute account move advance for the invoice with copago
        """
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        # Date = pool.get('ir.date')
        # today = Date.today()

        accounting_date = self.accounting_date or self.invoice_date
        period_id = Period.find(self.company.id, date=accounting_date)
        line = self.get_move_line_advance_copago()
        copago_line = self.get_copago_line()

        move = Move()
        move.journal = self.journal
        move.period = period_id
        move.date = accounting_date
        # move.origin = self
        move.company = self.company
        move.lines = [line, copago_line]
        return move

    def get_move_line_advance_copago(self):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        line = MoveLine()
        amount = self.copago
        line.amount_second_currency = None
        line.second_currency = None
        line.account = self.account
        if amount >= 0:
            line.debit, line.credit = 0, amount
        else:
            line.debit, line.credit = -amount, 0
        if self.account.party_required:
            line.party = self.party
        line.operation_center = self.operation_center.id
        return line

    @fields.depends('annexes', 'copago', 'cuota_moderadora')
    def on_change_annexes(self, name=None):
        self.copago = sum((a.copago or 0) for a in self.annexes)
        self.cuota_moderadora = sum((a.cuota_moderadora or 0) for a in self.annexes)


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @classmethod
    def _get_origin(cls):
        models = super()._get_origin()
        models.append('laboratory.order.line')
        return models


class InvoiceLaboratoryOrder(ModelSQL):
    "Invoice - Order"
    __name__ = "account.invoice-laboratory.order"
    _table = 'account_invoice_laboratory_order'

    invoice = fields.Many2One('account.invoice', 'Invoice',
        ondelete='CASCADE', required=True)
    order = fields.Many2One('laboratory.order', 'Laboratory Order',
        ondelete='RESTRICT', required=True)


class AdvancePaymentAsk(metaclass=PoolMeta):
    "Advance Payment Ask"
    __name__ = 'account.invoice.advance_payment.ask'

    @classmethod
    def default_lines(cls):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        invoice = Invoice(Transaction().context.get('active_id'))
        Config = pool.get('laboratory.configuration')
        config = Config.get_configuration()
        advance_accounts = config.advance_accounts.id
        move_lines = []
        for annexe in invoice.annexes:
            if annexe.copago_invoice and annexe.copago_invoice.move:
                move_lines.extend([line.id for line in annexe.copago_invoice.move.lines if line.account.id == advance_accounts])
        return move_lines
