
-- Actualizar cups en lineas de ordenes de servicio
UPDATE laboratory_order_line SET code=my_table.test_code
FROM (SELECT lol.id AS line_id, lol.code AS line_code, ltt.code AS test_code
FROM laboratory_order_line AS lol
JOIN laboratory_test_type AS ltt
ON ltt.product=lol.test
WHERE lol.create_date > '2024-09-01') AS my_table
WHERE my_table.line_id=laboratory_order_line.id;
