# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta


class Section(ModelSQL, ModelView):
    'Product Section'
    __name__ = 'product.section'
    name = fields.Char('Name')


class Conservation(ModelSQL, ModelView):
    'Product Conservation'
    __name__ = 'product.conservation'
    name = fields.Char('Name')


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    section = fields.Many2One('product.section', 'Section')
    conservation = fields.Many2One('product.conservation', 'Conservation')

class ProductSupplier(metaclass=PoolMeta):
    __name__ = 'purchase.product_supplier'

    invima = fields.Char('Invima')
    date_invima = fields.Date('Date Invima')
    risk_type = fields.Selection([
            ('', ''),
            ('type_1', 'Type 1'),
            ('type_2', 'Type 2'),
            ('type_3', 'Type 3'),
            ('type_4', 'Type 4'),
            ('type_5', 'Type 5')
        ],'Risk Type')

    risk_type_string = risk_type.translated('risk_type')